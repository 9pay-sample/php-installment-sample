<?php
    require_once('lib/HMACSignature.php');
    require_once('lib/MessageBuilder.php');

    #Thông tin cấu hình
    const MERCHANT_KEY = '4eSVyu'; // thông tin key của merchant
    const MERCHANT_SECRET_KEY = 'NUxBpfNh2IBRvoU4CnJUHuOf0M0yN6kfDlJ';  // thông tin secret key của merchant
    const END_POINT = 'https://sand-payment.9pay.vn';

    $invoiceNo = time() + rand(0,999999);
    $amount = 3000000;
    $description = "Mô tả giao dịch";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
        $returnUrl = str_replace('index.php', '', $backUrl);
        $time = time();
        $data = array(
            'merchantKey' => MERCHANT_KEY,           
            'time' => $time,
            'transaction_type' => 'INSTALLMENT',
            'invoice_no' => $_POST['invoice_no'],
            'amount' => $_POST['amount'],
            'description' => $_POST['description'],
            'return_url' => "{$returnUrl}result.php",
        );
        $message = MessageBuilder::instance()
            ->with($time, END_POINT . '/payments/create', 'POST')
            ->withParams($data)
            ->build();
        $hmacs = new HMACSignature();
        $signature = $hmacs->sign($message, MERCHANT_SECRET_KEY);
        $httpData = [
            'baseEncode' => base64_encode(json_encode($data, JSON_UNESCAPED_UNICODE)),
            'signature' => $signature,
        ];
        $redirectUrl = END_POINT . '/portal?' . http_build_query($httpData);
        return header('Location: ' . $redirectUrl);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Thanh toán trả góp</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-6 offset-3 border mt-5 p-3" id="form_payment">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="payment_no">Mã giao dịch</label>
                            <input type="text" class="form-control" name="invoice_no" value="<?= $invoiceNo ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="amount">Số tiền</label>
                            <input type="text" name="amount" class="form-control" value="<?= $amount ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <input type="text" name="description" class="form-control" value="<?= $description ?>" placeholder="Mô tả giao dich" required>
                        </div>

                        <div class="action text-center">
                            <button type="submit" class="btn btn-success">Thanh toán</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>